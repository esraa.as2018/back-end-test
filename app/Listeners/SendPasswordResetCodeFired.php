<?php

namespace App\Listeners;

use App\Events\SendPasswordResetCode;
use App\Jobs\SendEmailActiveCodeJob;
use App\Jobs\SendPasswordResetCodeJob;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendPasswordResetCodeFired
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(SendPasswordResetCode $event): void
    {
        dispatch(new SendPasswordResetCodeJob($event->user));
    }
}
