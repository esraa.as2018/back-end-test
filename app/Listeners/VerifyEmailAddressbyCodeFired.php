<?php

namespace App\Listeners;

use App\Events\VerifyEmailAddressByCode;
use App\Jobs\SendEmailActiveCodeJob;
use App\Mail\SendEmailActiveCode;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class VerifyEmailAddressbyCodeFired
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(VerifyEmailAddressByCode $event): void
    {
        dispatch(new SendEmailActiveCodeJob($event->user));
    }
}
