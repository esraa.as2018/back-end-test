<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\PaginatedCollection;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\Concerns\Has;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $users = User::query()->where('role', '=', 'user')->paginate(20);
        return $request->expectsJson()
            ? apiResponse(PaginatedCollection::collection(UserResource::collection($users)), 'all users')
            : view('dashboard.users', compact('users'));
    }

    public function getUserProduct(Request $request, User $user){
        return view('dashboard.user_products', [
            'products'=>$user->products()->paginate(20),
            'user'=> $user
        ]);
    }

    public function store(UserStoreRequest $request)
    {
        $user = User::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'phone' => $request['phone'],
            'email' => $request['email'],
            'email_code' => rand(0000, 9999),
            'password' => Hash::make($request['password']),
            'role' => 'user',
            'register_type' => 'phone',
        ]);
        return $request->expectsJson()
            ? apiResponse(new UserResource($user), 'user has been created successfully')
            : back()->with('success','user info has been created successfully');
    }

    public function show(User $user)
    {
        return apiResponse(new UserResource($user), 'show user information');
    }

    public function update(UserUpdateRequest $request, User $user)
    {
        $user->update([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'phone' => $request['phone'],
            'email' => $request['email'],
        ]);
        return $request->expectsJson()
            ? apiResponse(new UserResource($user), 'user info has been updated successfully')
            : back()->with('success','user info has been updated successfully');
    }

    public function destroy(Request $request, User $user)
    {
        $user->delete();
        return $request->expectsJson()
            ? apiResponse(null, 'user has been deleted successfully')
            : back()->with('success','user info has been deleted successfully') ;
    }
}
