<?php

namespace App\Http\Controllers\ApiAuth;

use App\Events\VerifyEmailAddressByCode;
use App\Http\Controllers\Controller;
use App\Http\Requests\ActiveAccountRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class ActiveAccountController extends Controller
{
    public function resendActivateCode()
    {
        event(new VerifyEmailAddressByCode(Auth::user()));
        return apiResponse(
            null,
            'The activation code will be re-sent to your email within a few seconds.'.
            'If you do not receive the activation code, you can request it again.'
        );
    }

    public function activeAccount(ActiveAccountRequest $request)
    {
        $user = Auth::user();
        if ($user->email_verified_at == null && $user->email_code == $request['email_code']) {
            $user->update([
                'email_verified_at' => now(),
                'email_code' => null
            ]);
            return apiResponse(null, 'The account has been activated successfully');
        }
        throw ValidationException::withMessages([
            'fail' => 'The activation code is invalid'
        ]);
    }
}
