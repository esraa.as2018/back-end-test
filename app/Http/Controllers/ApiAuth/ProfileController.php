<?php

namespace App\Http\Controllers\ApiAuth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileUpdateRequest;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{


    public function profile(Request $request)
    {
        return $request->expectsJson()
            ? apiResponse(new UserResource(Auth::user()), 'Your account information')
            : view('dashboard.profile', ['user' => Auth::user()]);
    }

    public function update(ProfileUpdateRequest $request)
    {
        $user = Auth::user();
        $user->update([
            'first_name' => $request['first_name'] ?? $user->first_name,
            'last_name' => $request['last_name'] ?? $user->last_name,
            'email' => $request['email'] ?? $user->email,
            'phone' => $request['phone'] ?? $user->phone,
        ]);
        return $request->expectsJson()
            ? apiResponse(
                new UserResource($user),
                'Your account information has been successfully modified'
            )
            : back()->with('success', 'Your account information has been successfully modified');
    }
}
