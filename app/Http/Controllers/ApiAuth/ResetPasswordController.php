<?php

namespace App\Http\Controllers\ApiAuth;

use App\Events\SendPasswordResetCode;
use App\Http\Controllers\Controller;
use App\Http\Requests\ResetPasswordStoreRequest;
use App\Http\Requests\ResetPasswordIndexRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class ResetPasswordController extends Controller
{
    public function index(ResetPasswordIndexRequest $request)
    {
        $user = $request->has('email')
            ? User::where('email', $request->email)->first()
            : User::where('phone', $request->phone)->first();
        $user->update([
            'reset_password_code' => rand(1000, 9999)
        ]);
        event(new SendPasswordResetCode($user));
        return apiResponse(
            null,
            'The reset password code will be sent to your email within a few seconds. If you do not receive the reset password code, you can request it again.'
        );
    }

    public function store(ResetPasswordStoreRequest $request)
    {
        $user = $request->has('email')
            ? User::where('email', $request->email)->first()
            : User::where('phone', $request->phone)->first();
        if ($user->reset_password_code != null && $user->reset_password_code ==
            $request['reset_password_code']) {
            $user->update([
                'reset_password_code' => null,
                'password' => Hash::make($request['new_password']),
            ]);
            return apiResponse(null, 'The password has been reset successfully');
        }
        throw ValidationException::withMessages([
            'fail' => 'The reset password code is invalid'
        ]);
    }
}
