<?php

namespace App\Http\Controllers\ApiAuth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChangePasswordController extends Controller
{
    public function changePassword(ChangePasswordRequest $request)
    {
        $user = Auth::user();
        $user->update(['password' => $request->new_password]);
        return $request->expectsJson()
            ? apiResponse(new UserResource($user), 'The password has been changed successfully')
            : back()->with('success','The password has been changed successfully');
    }
}
