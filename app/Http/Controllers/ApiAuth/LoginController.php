<?php

namespace App\Http\Controllers\ApiAuth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function login(LoginRequest $request)
    {
        $user = $request->has('email')
            ? User::where('email', $request->email)->first()
            : User::where('phone', $request->phone)->first();
        if ($user && Hash::check($request['password'], $user['password'])) {
            $token = $user->createToken('token')->plainTextToken;
            return apiResponse(
                [
                    'user' => new UserResource($user),
                    'token' => $token
                ],
                'You have been logged in successfully',
                200
            );
        }
        throw ValidationException::withMessages([
            'password' => 'The password is incorrect',
        ]);
    }
}
