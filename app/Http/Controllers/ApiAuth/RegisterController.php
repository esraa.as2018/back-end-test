<?php

namespace App\Http\Controllers\ApiAuth;

use App\Events\VerifyEmailAddressByCode;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function register(RegisterRequest $request)
    {
        $user = User::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'phone' => $request['phone'],
            'register_type' => $request['register_type'],
            'email_code' => rand(1000, 9999),
        ]);
        if ($request['register_type'] == 'email')
            event(new VerifyEmailAddressByCode($user));
        $token = $user->createToken('token')->plainTextToken;
        return apiResponse(
            [
                'user' => new UserResource($user),
                'token' => $token
            ],
            'Registered successfully',
            201
        );
    }
}
