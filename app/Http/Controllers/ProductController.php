<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductAssignToUserRequest;
use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Resources\PaginatedCollection;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{

    public function index(Request $request)
    {
        $products = Product::query()->paginate(10);
        return $request->expectsJson()
            ? apiResponse(new PaginatedCollection
            (ProductResource::collection($products)),
                'all products')
            : view('dashboard.products', [
                'products'=>$products,
                'users'=>User::query()->where('role','=','user')->get()
            ]);
    }

    public function getUserProducts()
    {
        $products = Product::query()
            ->where('user_id', Auth::id())
            ->paginate(20);
        return apiResponse(new PaginatedCollection(ProductResource::collection($products)));
    }

    public function store(ProductStoreRequest $request)
    {
        $product = Product::create([
            'name' => $request->name,
            'description' => $request->description,
            'image' => $request['image'] ? $request['image']->store('products',
                'public') : null
        ]);
        return $request->expectsJson()
            ? apiResponse(new ProductResource($product), 'product has been created successfully')
            : back()->with('success', 'product has been created successfully');
    }

    public function show(Product $product)
    {
        return apiResponse(new ProductResource($product), 'show product');
    }

    public function update(ProductUpdateRequest $request, Product $product)
    {
        $image = $product->image;
        if ($request->has('image')) {
            if ($image != null) Storage::disk('public')->delete($image);
            $image = $request['image']->store('products', 'public');
        }
        $product->update([
            'name' => $request->name ? $request->name : $product->name,
            'description' => $request->description ? $request->description : $product->description,
            'image' => $image
        ]);
        return $request->expectsJson()
            ? apiResponse(new ProductResource($product), 'product has been updated successfully')
            : back()->with('success', 'product has been updated successfully');
    }

    public function destroy(Request $request, Product $product)
    {
        $product->delete();
        return $request->expectsJson()
            ? apiResponse(null, 'product has been deleted successfully')
            : back()->with('success', 'product has been deleted successfully');
    }

    public function assignToUser(ProductAssignToUserRequest $request, Product $product)
    {
        $product->update([
            'user_id' => $request['user_id']
        ]);
        return $request->expectsJson()
            ? apiResponse(new ProductResource($product),
                'The product has been successfully assigned to the user')
            : back()->with('success', 'The product has been successfully assigned to the user');
    }
}
