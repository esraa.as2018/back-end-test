<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProductAssignToUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Auth::user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'user_id'=>['required','exists:users,id']
        ];
    }

    public function withValidator($validator){
        $validator->validate();
        $validator->after(function ($validator){
            if (User::query()->where('id',$this->user_id)->first()->isAdmin()){
                $validator->errors()->add(
                    'fail',
                    'you can not assign product to admin'
                );
            }
        });
    }
}
