<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ResetPasswordStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'email'=>[Rule::requiredIf($this->phone == null),'email','exists:users,email'],
            'phone'=>[Rule::requiredIf($this->email == null),'regex:/^([0-9\s\-\+\(\)]*)$/','exists:users,phone'],
            'reset_password_code'=> ['required', 'string','min:4','max:4'],
            'new_password'=> ['required', 'string', 'min:8'],
        ];
    }
}
