<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Symfony\Component\HttpFoundation\Response;

class EnsureEmailIsVerifiedIfUserRegisterByEmail
{

    public function handle(Request $request, Closure $next): Response
    {

        if (!$request->user() ||
            ($request->user() instanceof MustVerifyEmail &&
                !$request->user()->hasVerifiedEmail() && $request->user()
                    ->register_type == 'email') && $request->user()->role != 'admin') {
            abort(403, 'Your email address is not verified.');
        }
        return $next($request);
    }
}
