<?php

namespace App\Providers;

use App\Events\SendPasswordResetCode;
use App\Events\VerifyEmailAddressByCode;
use App\Jobs\SendPasswordResetCodeJob;
use App\Listeners\SendPasswordResetCodeFired;
use App\Listeners\VerifyEmailAddressbyCodeFired;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        VerifyEmailAddressByCode::class=>[
            VerifyEmailAddressbyCodeFired::class
        ],
        SendPasswordResetCode::class=>[
            SendPasswordResetCodeFired::class
        ]
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
