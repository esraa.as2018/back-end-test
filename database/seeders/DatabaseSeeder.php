<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        User::create([
            'first_name'=>'admin',
            'last_name'=>'admin',
            'phone'=>'+96399999999',
            'email'=>'admin@example.com',
            'password'=>Hash::make('12345678'),
            'role'=>'admin',
            'register_type'=>'email',
            'email_verified_at'=>now()
        ]);

        User::create([
            'first_name'=>'test',
            'last_name'=>'test',
            'phone'=>'+963999999996',
            'email'=>'test@example.com',
            'password'=>Hash::make('12345678'),
            'role'=>'user',
            'register_type'=>'email',
            'email_verified_at'=>now()
        ]);
    }
}
