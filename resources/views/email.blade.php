<x-mail::message>

    <h3>{{$title}}</h3>
    <p>
        {{$content}}
    </p>
Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
