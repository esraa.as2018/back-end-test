@extends('dashboard.layouts.master')

@section('css')

@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card border shadow-xs mb-4">
                <div class="card-header border-bottom pb-0">
                    <div class="d-sm-flex align-items-center">
                        <div>
                            <h6 class="font-weight-semibold text-lg
                            mb-0">Product
                                list</h6>
                            <p class="text-sm">See information about all
                                products assign to user {{$user->first_name}} {{$user->last_name}}</p>
                        </div>
                    </div>
                </div>
                <div class="card-body px-0 py-0">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead class="bg-gray-100">
                            <tr>
                                <th class="text-secondary text-xs
                                font-weight-semibold opacity-7">Name
                                </th>
                                <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td>
                                        <div class="d-flex px-2 py-1">
                                            <div class="d-flex flex-column justify-content-center ms-1">
                                                <div class="d-flex align-items-center">
                                                    @if($product->image != null)
                                                        <img src="{{asset('storage/'.$product->image)}}"
                                                             class="avatar avatar-sm rounded-circle me-2"
                                                             alt="product">
                                                    @endif
                                                    <h6 class="mb-0 text-sm
                                                font-weight-semibold">{{$product->name}}</h6>
                                                </div>

                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <p class="text-sm text-dark
                                        font-weight-semibold
                                        mb-0">{{$product->description}}</p>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="p-3">
                        {{$products->links('vendor.pagination.bootstrap-5')}}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('.users').addClass('active');
        })
    </script>
@endsection
