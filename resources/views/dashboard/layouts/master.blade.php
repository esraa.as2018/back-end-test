<html lang="en">
<head>
    @include('dashboard.layouts.head')
    <title>Admin Dashboard</title>
</head>
<body class="g-sidenav-show  bg-gray-100">
@include('dashboard.layouts.side-bar')
<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    @include('dashboard.layouts.nav-bar')
    @if(session()->has('success'))
        <div class="alert alert-success flash-message">
            {{ session()->get('success') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger flash-message">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container-fluid py-4 px-5">
        @yield('content')
        @include('dashboard.layouts.footer')
    </div>
</main>
@include('dashboard.layouts.script')
</body>
</html>
