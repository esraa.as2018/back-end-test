@extends('dashboard.layouts.master')

@section('css')

@endsection

@section('content')
    <div class="row">
        <div class="col-12>
            <div class=" card border shadow-xs h-100
        ">
        <div class="card-header pb-0 p-3">
            <div class="row">
                <div class="col-md-8 col-9">
                    <h6 class="mb-0 font-weight-semibold text-lg">User
                        information</h6>
                    <p class="text-sm mb-1">Edit the user information </p>
                </div>
                <div class="col-md-4 col-3 text-end">
                    <button data-bs-toggle="modal"
                            data-bs-target="#editUserPassModal" type="button"
                            class="btn btn-white btn-icon px-2 py-2">
                        <i class=" fa fa-key"></i>
                    </button>
                    <button data-bs-toggle="modal"
                            data-bs-target="#editUserInfoModal" type="button"
                            class="btn btn-white btn-icon px-2 py-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14"
                             height="14" viewBox="0 0 24 24"
                             fill="currentColor">
                            <path
                                d="M21.731 2.269a2.625 2.625 0 00-3.712 0l-1.157 1.157 3.712 3.712 1.157-1.157a2.625 2.625 0 000-3.712zM19.513 8.199l-3.712-3.712-12.15 12.15a5.25 5.25 0 00-1.32 2.214l-.8 2.685a.75.75 0 00.933.933l2.685-.8a5.25 5.25 0 002.214-1.32L19.513 8.2z"/>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
        <div class="card-body p-3">
            <ul class="list-group">
                <li class="list-group-item border-0 ps-0 text-dark font-weight-semibold pt-0 pb-1 text-sm"><span
                        class="text-secondary">First Name:</span>
                    &nbsp;
                    {{$user->first_name}}</li>
                <li class="list-group-item border-0 ps-0 text-dark font-weight-semibold pt-0 pb-1 text-sm"><span
                        class="text-secondary">Last Name:</span>
                    &nbsp;
                    {{$user->last_name}}</li>
                <li class="list-group-item border-0 ps-0 text-dark font-weight-semibold pb-1 text-sm"><span
                        class="text-secondary">Email:</span>
                    &nbsp; {{$user->email}}</li>
                <li class="list-group-item border-0 ps-0 text-dark font-weight-semibold pb-1 text-sm"><span
                        class="text-secondary">Phone:</span> &nbsp;
                    {{$user->phone}}</li>
            </ul>
        </div>
    </div>

    {{--edit user info modal--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="editUserInfoModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content borer-top border-primary">
                <form action="{{route('dashboard.profile.update')}}" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="modal-body">
                        <div class="">
                            <h4 class="h5">Edit User information</h4>
                            <p class="p-0 mb-1 text-secondary">Don't Miss Any
                                Updates</p>
                            <hr>
                        </div>
                        <div class="p-3">
                            <div class="row">
                                <div class="col-12">
                                    <label for="first_name">First Name</label>
                                    <input type="text" id="first_name"
                                           name="first_name"
                                           required
                                           value="{{$user->first_name}}"
                                           placeholder="Enter first name"
                                           class="form-control rounded-2 mt-2 mb-2">
                                </div>
                                <div class="col-12">
                                    <label for="last_name">First Name</label>
                                    <input type="text" id="last_name"
                                           name="last_name"
                                           required
                                           value="{{$user->last_name}}"
                                           placeholder="Enter last name"
                                           class="form-control rounded-2 mt-2 mb-2">
                                </div>
                                <div class="col-12">
                                    <label for="email">Email</label>
                                    <input type="text" id="email" name="email"
                                           required
                                           value="{{$user->email}}"
                                           placeholder="Enter email"
                                           class="form-control rounded-2 mt-2 mb-2">
                                </div>
                                <div class="col-12">
                                    <label for="phone">Phone</label>
                                    <input type="text" id="phone" name="phone"
                                           required
                                           value="{{$user->phone}}"
                                           placeholder="Enter phone"
                                           class="form-control rounded-2 mt-2 mb-2">
                                </div>
                            </div>
                        </div>
                        <div class="p-3 row">
                            <div class="col-6">
                                <a type="button"
                                   class="btn btn-secondary rounded-0 w-100"
                                   data-bs-dismiss="modal"><small>Cancel</small></a>
                            </div>
                            <div class="col-6">
                                <button type="submit"
                                        class="btn btn-primary text-white rounded-0 w-100">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

    {{--edit user password modal--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="editUserPassModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content borer-top border-primary">
                <form action="{{route('dashboard.profile.change-password')}}"
                      method="post"
                      enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="modal-body">
                        <div class="">
                            <h4 class="h5">Edit User information</h4>
                            <p class="p-0 mb-1 text-secondary">Don't Miss Any
                                Updates</p>
                            <hr>
                        </div>
                        <div class="p-3">
                            <div class="row">
                                <div class="col-12">
                                    <label for="old_password">Old
                                        password</label>
                                    <input type="password" id="old_password"
                                           name="old_password" required
                                           placeholder="Enter old password"
                                           class="form-control rounded-2 mt-2 mb-2">
                                </div>
                                <div class="col-12">
                                    <label for="new_password">New
                                        password</label>
                                    <input type="password" id="new_password"
                                           name="new_password" required
                                           placeholder="Enter new password"
                                           class="form-control rounded-2 mt-2 mb-2">
                                </div>
                            </div>
                        </div>
                        <div class="p-3 row">
                            <div class="col-6">
                                <a type="button"
                                   class="btn btn-secondary rounded-0 w-100"
                                   data-bs-dismiss="modal"><small>Cancel</small></a>
                            </div>
                            <div class="col-6">
                                <button type="submit"
                                        class="btn btn-primary text-white rounded-0 w-100">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('.profile').addClass('active');
    })
</script>
@endsection
