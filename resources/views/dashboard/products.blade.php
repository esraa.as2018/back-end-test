@extends('dashboard.layouts.master')

@section('css')

@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card border shadow-xs mb-4">
                <div class="card-header border-bottom pb-0">
                    <div class="d-sm-flex align-items-center">
                        <div>
                            <h6 class="font-weight-semibold text-lg
                            mb-0">Product
                                list</h6>
                            <p class="text-sm">See information about all
                                products</p>
                        </div>
                        <div class="ms-auto d-flex">
                            <button type="button" class="btn btn-sm btn-dark btn-icon d-flex align-items-center me-2"
                                    data-bs-toggle="modal" data-bs-target="#addModal">
                                <span><i class="fa fa-plus-circle"
                                         style="font-size: 1rem; padding-right:10px"></i></span>
                                <span class="btn-inner--text">Add product</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body px-0 py-0">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead class="bg-gray-100">
                            <tr>
                                <th class="text-secondary text-xs
                                font-weight-semibold opacity-7">Name
                                </th>
                                <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">Description</th>
                                <th class="text-secondary text-xs font-weight-semibold opacity-7 ps-2">Assign to</th>
                                <th class=" text-secondary text-xs font-weight-semibold opacity-7"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td>
                                        <div class="d-flex px-2 py-1">
                                            <div class="d-flex flex-column justify-content-center ms-1">
                                                <div class="d-flex align-items-center">
                                                    @if($product->image != null)
                                                        <img src="{{asset('storage/'.$product->image)}}"
                                                             class="avatar avatar-sm rounded-circle me-2"
                                                             alt="product">
                                                    @endif
                                                    <h6 class="mb-0 text-sm
                                                font-weight-semibold">{{$product->name}}</h6>
                                                </div>

                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <p class="text-sm text-dark
                                        font-weight-semibold
                                        mb-0">{{$product->description}}</p>

                                    </td>
                                    <td>
                                        <p class="text-sm text-dark
                                        font-weight-semibold
                                        mb-0">{{$product->user_id != null ? $product->user->first_name. ' ' .
                                        $product->user->last_name : 'not assign yet'}}</p>

                                    </td>
                                    <td class="align-middle">
                                        <a href="javascript:;" class="text-secondary font-weight-bold text-xs edit"
                                           data-id="{{$product->id}}"
                                           data-name="{{$product->name}}"
                                           data-description="{{$product->description}}"
                                           data-bs-toggle="modal"
                                           data-bs-target="#editModal"
                                           data-bs-title="Edit product">
                                            <svg width="14" height="14" viewBox="0 0 15 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M11.2201 2.02495C10.8292 1.63482 10.196 1.63545 9.80585 2.02636C9.41572 2.41727 9.41635 3.05044 9.80726 3.44057L11.2201 2.02495ZM12.5572 6.18502C12.9481 6.57516 13.5813 6.57453 13.9714 6.18362C14.3615 5.79271 14.3609 5.15954 13.97 4.7694L12.5572 6.18502ZM11.6803 1.56839L12.3867 2.2762L12.3867 2.27619L11.6803 1.56839ZM14.4302 4.31284L15.1367 5.02065L15.1367 5.02064L14.4302 4.31284ZM3.72198 15V16C3.98686 16 4.24091 15.8949 4.42839 15.7078L3.72198 15ZM0.999756 15H-0.000244141C-0.000244141 15.5523 0.447471 16 0.999756 16L0.999756 15ZM0.999756 12.2279L0.293346 11.5201C0.105383 11.7077 -0.000244141 11.9624 -0.000244141 12.2279H0.999756ZM9.80726 3.44057L12.5572 6.18502L13.97 4.7694L11.2201 2.02495L9.80726 3.44057ZM12.3867 2.27619C12.7557 1.90794 13.3549 1.90794 13.7238 2.27619L15.1367 0.860593C13.9869 -0.286864 12.1236 -0.286864 10.9739 0.860593L12.3867 2.27619ZM13.7238 2.27619C14.0917 2.64337 14.0917 3.23787 13.7238 3.60504L15.1367 5.02064C16.2875 3.8721 16.2875 2.00913 15.1367 0.860593L13.7238 2.27619ZM13.7238 3.60504L3.01557 14.2922L4.42839 15.7078L15.1367 5.02065L13.7238 3.60504ZM3.72198 14H0.999756V16H3.72198V14ZM1.99976 15V12.2279H-0.000244141V15H1.99976ZM1.70617 12.9357L12.3867 2.2762L10.9739 0.86059L0.293346 11.5201L1.70617 12.9357Z"
                                                    fill="#64748B"/>
                                            </svg>
                                        </a>
                                        <a href="javascript:;" class="text-danger font-weight-bold text-xs delete pl-1 "
                                           data-id="{{$product->id}}"
                                           data-bs-toggle="modal" data-bs-target="#deleteModal"
                                           data-bs-title="Delete service">
                                            <svg width="14" height="14" xmlns="http://www.w3.org/2000/svg"
                                                 xml:space="preserve" viewBox="0 0 16 16"><g fill="currentColor">
                                                    <path d="M7 5H6v8h1zM10 5H9v8h1z"/>
                                                    <path
                                                        d="M13 3h-2v-.75C11 1.56 10.44 1 9.75 1h-3.5C5.56 1 5 1.56 5 2.25V3H3v10.75c0 .69.56 1.25 1.25 1.25h7.5c.69 0 1.25-.56 1.25-1.25V3zm-7-.75A.25.25 0 0 1 6.25 2h3.5a.25.25 0 0 1 .25.25V3H6v-.75zm6 11.5a.25.25 0 0 1-.25.25h-7.5a.25.25 0 0 1-.25-.25V4h8v9.75z"/>
                                                    <path d="M13.5 4h-11a.5.5 0 0 1 0-1h11a.5.5 0 0 1 0 1z"/>
                                                </g></svg>

                                        </a>
                                        <a href="javascript:;"
                                           class="text-success font-weight-bold text-xs assign pl-1 "
                                           data-id="{{$product->id}}"
                                           data-bs-toggle="modal" data-bs-target="#assignModal"
                                           data-bs-title="assign to user">
                                            Assign To User

                                        </a>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="p-3">
                        {{$products->links('vendor.pagination.bootstrap-5')}}

                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--edit modal--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="editModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content borer-top border-primary">
                <form id="update_form" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="modal-body">
                        <div class="">
                            <h4 class="h5">Edit Product</h4>
                            <p class="p-0 mb-1 text-secondary">Don't Miss Any Updates</p>
                            <hr>
                        </div>
                        <div class="p-3">
                            <div class="row">
                                <div class="col-12">
                                    <label for="name">Name</label>
                                    <input type="text" id="name"
                                           name="name"
                                           required
                                           placeholder="Enter Name"
                                           class="form-control rounded-2 mt-2 mb-2">
                                </div>
                                <div class="col-12">
                                    <label for="description">Description</label>
                                    <input type="text" id="description"
                                           name="description"
                                           required
                                           placeholder="Enter description"
                                           class="form-control rounded-2 mt-2 mb-2">
                                </div>
                                <div class="col-12">
                                    <label for="image">Update Image if
                                        you want</label>
                                    <input id="image" name="image" type="file" accept="image/*"
                                           class="form-control rounded-2 mt-2 mb-2">
                                </div>
                            </div>
                        </div>
                        <div class="p-3 row">
                            <div class="col-6">
                                <a type="button" class="btn btn-secondary rounded-0 w-100"
                                   data-bs-dismiss="modal"><small>Cancel</small></a>
                            </div>
                            <div class="col-6">
                                <button type="submit" class="btn btn-primary text-white rounded-0 w-100">Save</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

    {{--add modal--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="addModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content borer-top border-primary">
                <form action="{{route('dashboard.products.store')}}"
                      method="post"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="">
                            <h4 class="h5">Add product</h4>
                            <p class="p-0 mb-1 text-secondary">Don't Miss Any Fields</p>
                            <hr>
                        </div>
                        <div class="p-3">
                            <div class="row">
                                <div class="col-12">
                                    <label for="name_add">
                                        name</label>
                                    <input type="text" id="name_add"
                                           name="name" required
                                           placeholder="Enter Name"
                                           class="form-control rounded-2 mt-2 mb-2">
                                </div>
                                <div class="col-12">
                                    <label for="description_add">Description</label>
                                    <input type="text" id="description_add"
                                           name="description"
                                           placeholder="Enter description" required
                                           class="form-control rounded-2 mt-2 mb-2">
                                </div>
                                <div class="col-12">
                                    <label for="image">Image</label>
                                    <input name="image" type="file" accept="image/*"
                                           class="form-control rounded-2 mt-2 mb-2">
                                </div>
                            </div>
                        </div>
                        <div class="p-3 row">
                            <div class="col-6">
                                <a type="button" class="btn btn-secondary rounded-0 w-100"
                                   data-bs-dismiss="modal"><small>Cancel</small></a>
                            </div>
                            <div class="col-6">
                                <button type="submit" class="btn btn-primary text-white rounded-0 w-100">Add</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="assignModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content borer-top border-primary">
                <form
                    id="assign_form"
                    method="post"
                    enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="modal-body">
                        <div class="">
                            <h4 class="h5">Assign product to user</h4>
                            <hr>
                        </div>
                        <div class="p-3">
                            <div class="row">
                                <div class="col-12">
                                    <label >
                                        user</label>
                                    <select class="form-select" name="user_id">
                                        <option value="">select one</option>

                                        @foreach($users as $user)
                                            <option
                                                value="{{$user->id}}">{{$user->first_name. ' ' .$user->last_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="p-3 row">
                            <div class="col-6">
                                <a type="button" class="btn btn-secondary rounded-0 w-100"
                                   data-bs-dismiss="modal"><small>Cancel</small></a>
                            </div>
                            <div class="col-6">
                                <button type="submit" class="btn btn-primary text-white rounded-0 w-100">Assign</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>


    {{--delete modal--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="deleteModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content borer-top border-danger">
                <form id="delete_form" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('delete')
                    <div class="modal-body">
                        <div class="">
                            <h4 class="h5">Delete product</h4>
                            <hr>
                        </div>
                        <div class="p-3">
                            <p>Are you sure to delete this product?</p>
                        </div>
                        <div class="p-3 row">
                            <div class="col-6">
                                <a type="button" class="btn btn-secondary rounded-0 w-100"
                                   data-bs-dismiss="modal"><small>Cancel</small></a>
                            </div>
                            <div class="col-6">
                                <button type="submit" class="btn btn-danger text-white rounded-0 w-100">Delete</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('.products').addClass('active');
            $('.edit').click(function () {
                $('#name').val($(this).data('name'));
                $('#description').val($(this).data('description'));
                $('#email').val($(this).data('email'));
                $('#phone').val($(this).data('phone'));
                $('#update_form').attr('action', `/dashboard/products/${$(this)
                    .data('id')}`)
            });
            $('.delete').click(function () {
                $('#delete_form').attr('action', `/dashboard/products/${$(this)
                    .data('id')}`)
            });
            $('.assign').click(function () {
                $('#assign_form').attr('action', `/dashboard/products/${$(this).data('id')}/assignToUser`)
            });

        })
    </script>
@endsection
