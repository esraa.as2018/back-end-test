<?php

use App\Http\Controllers\ApiAuth\ActiveAccountController;
use App\Http\Controllers\ApiAuth\ChangePasswordController;
use App\Http\Controllers\ApiAuth\LoginController;
use App\Http\Controllers\ApiAuth\LogoutController;
use App\Http\Controllers\ApiAuth\ProfileController;
use App\Http\Controllers\ApiAuth\RegisterController;
use App\Http\Controllers\ApiAuth\ResetPasswordController;
use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', [RegisterController::class, 'register']);
Route::post('/login', [LoginController::class, 'login']);
Route::get('/reset-password', [ResetPasswordController::class, 'index']);
Route::patch('/reset-password', [ResetPasswordController::class, 'store']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::delete('/logout', [LogoutController::class, 'logout']);
    Route::get('/resend-activate-email-code', [ActiveAccountController::class, 'resendActivateCode']);
    Route::patch('/active-account', [ActiveAccountController::class, 'activeAccount']);
    Route::group(['middleware' => 'ensureEmailIsVerifiedIfUserRegisterByEmail'], function () {
        Route::put('/change-password', [ChangePasswordController::class, 'changePassword']);
        Route::get('/profile', [ProfileController::class, 'profile']);
        Route::put('/profile', [ProfileController::class, 'update']);
        Route::group(['middleware' => 'role:admin', 'prefix' => 'dashboard'], function () {
            Route::apiResource('products', ProductController::class);
            Route::put('products/{product}/assignToUser',
                [ProductController::class, 'assignToUser']);
        });
        Route::group(['middleware' => 'role:user'], function () {
            Route::get('products', [ProductController::class, 'getUserProducts']);
        });
    });

});
