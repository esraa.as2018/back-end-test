<?php

use App\Http\Controllers\ApiAuth\ChangePasswordController;
use App\Http\Controllers\ApiAuth\ProfileController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return redirect('dashboard/products');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {

    Route::group(['middleware' => 'role:admin', 'prefix' => 'dashboard',
        'as' => 'dashboard.'],
        function () {
            Route::get('/profile', [ProfileController::class, 'profile'])->name('profile');
            Route::put('/profile', [ProfileController::class, 'update'])->name('profile.update');
            Route::put('/change-password', [ChangePasswordController::class, 'changePassword'])->name('profile.change-password');
            Route::resource('products', ProductController::class)
                ->only(['index', 'store', 'update', 'destroy']);
            Route::resource('users', UserController::class)
                ->only(['index', 'store', 'update', 'destroy']);
            Route::get('users/{user}/products', [UserController::class, 'getUserProduct'])
                ->name('user.products');
            Route::put('products/{product}/assignToUser',
                [ProductController::class, 'assignToUser']);
        });
});

require __DIR__ . '/auth.php';
